/*
Navicat MySQL Data Transfer

Source Server         : me
Source Server Version : 50521
Source Host           : localhost:3306
Source Database       : niceboat

Target Server Type    : MYSQL
Target Server Version : 50521
File Encoding         : 65001

Date: 2014-06-19 00:06:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('6', '1', '11', null);
INSERT INTO `article` VALUES ('7', '2', '22', null);
INSERT INTO `article` VALUES ('8', '3', '33', null);
INSERT INTO `article` VALUES ('9', '4', '44', null);
INSERT INTO `article` VALUES ('10', 'qqq', 'eee', '2013-11-22 23:23:02');
INSERT INTO `article` VALUES ('11', 'lkioky', 'ukkyumy u', '2013-11-29 11:29:54');
/*Table structure for table `t_system_menu` */

DROP TABLE IF EXISTS `t_system_menu`;

CREATE TABLE `t_system_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `parentid` int(11) DEFAULT '0' COMMENT '父菜单id',
  `menu_type` enum('导航菜单','Tab页菜单') NOT NULL DEFAULT '导航菜单',
  `sx` int(11) DEFAULT NULL COMMENT '顺序',
  `hidden` int(11) unsigned zerofill DEFAULT NULL COMMENT '是否隐藏',
  `memo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8;

/*Data for the table `t_system_menu` */

insert  into `t_system_menu`(`id`,`level`,`menu_name`,`url`,`parentid`,`menu_type`,`sx`,`hidden`,`memo`) values (5,1,'后台系统管理','',0,'导航菜单',20,00000000000,NULL),(34,2,'用户管理','/niceboat/jsp/user/listUser.jsp',5,'导航菜单',1,00000000000,NULL),(35,2,'角色管理','/system/role/role.action',5,'导航菜单',2,00000000000,NULL),(38,2,'菜单管理','/system/menu/menu!tree.action',5,'导航菜单',3,00000000000,NULL),(156,2,'代码管理','/system/codedic/codedic.action',5,'导航菜单',0,00000000000,NULL),(271,0,'资源管理','/system/authorize/authorize.action',5,'导航菜单',0,NULL,NULL);

/*Table structure for table `t_system_user` */

DROP TABLE IF EXISTS `t_system_user`;

CREATE TABLE `t_system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(50) DEFAULT NULL COMMENT '登录名',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `createtime` date DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4888 DEFAULT CHARSET=utf8;

/*Data for the table `t_system_user` */

insert  into `t_system_user`(`id`,`loginname`,`username`,`password`,`email`,`phone`,`createtime`) values (1,'2','3',NULL,NULL,NULL,NULL),(3,'4',NULL,NULL,NULL,NULL,NULL),(4875,'test','3',NULL,'test@163.com','13912345678',NULL),(4876,'5',NULL,NULL,NULL,NULL,NULL),(4877,'6',NULL,NULL,NULL,NULL,NULL),(4878,'7',NULL,NULL,NULL,NULL,NULL),(4879,'8',NULL,NULL,NULL,NULL,NULL),(4880,'9',NULL,NULL,NULL,NULL,NULL),(4881,'0',NULL,NULL,NULL,NULL,NULL),(4882,'11',NULL,NULL,NULL,NULL,NULL),(4883,'12',NULL,NULL,NULL,NULL,NULL),(4884,'13',NULL,NULL,NULL,NULL,NULL),(4885,'14',NULL,NULL,NULL,NULL,NULL),(4886,'15',NULL,NULL,NULL,NULL,NULL),(4887,'16',NULL,NULL,NULL,NULL,NULL);
