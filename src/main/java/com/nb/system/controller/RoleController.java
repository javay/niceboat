package com.nb.system.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.free.common.utils.web.Servlets;
import com.nb.common.utils.SearchFilter;
import com.nb.system.NewPager;
import com.nb.system.model.Role;
import com.nb.system.model.UserRole;
import com.nb.system.service.RoleService;

/**
 * 角色
 * 模板自动生成   for FreeUI
 * @author mefly
 *
 */
@Controller
@RequestMapping(value="/system/role")
public class RoleController {

	@Resource
	private RoleService service;
	
	/*
	 * 列表json
	 */
	@RequestMapping("/ajax_list")
	@ResponseBody
	public Map<String, Object> list(HttpServletRequest request,
			@RequestParam(value="page",defaultValue="1") int page,
			@RequestParam(value="pagesize",defaultValue="10") int pagesize ){
		
		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
		Map<String, SearchFilter> filters = SearchFilter.parse(searchParams);
		
		NewPager pager = new NewPager();
		pager.setPageNumber(page);
		pager.setPageSize(pagesize);
		pager.setFilters(filters);
		
		return service.queryPage(pager);
	}
	/*
	 * 录入
	 */
	@RequestMapping("/input")
	public String input(HttpServletRequest request,
			@RequestParam(value="id",defaultValue="0") int id){
		request.setAttribute("ob", service.fetch(id));
		
		return "jsp/system/input-role";	
	}
	
	/*
	 * 录入
	 */
	@RequestMapping("/inputRoleMenu")
	public String inputRoleMenu(HttpServletRequest request,
			@RequestParam(value="id",defaultValue="0") int id){
		request.setAttribute("ob", service.fetch(id));
		
		return "jsp/system/input-roleMenu";	
	}

	/*
	 * 得到role下所有user
	 */
	@RequestMapping("/fetchRoleUsers")
	@ResponseBody
	public List<UserRole> fetchRoleUsers(HttpServletRequest request,
			@RequestParam(value="id",defaultValue="0") int id){
		Role role = service.fetchRoleUsers(id);
		return role.getUserRoles();	
	}
	/*
	 * 保存
	 */
	@RequestMapping("/save")
	@ResponseBody
	public String save(Role role,
						HttpServletRequest request){
		if (role.getId()==null){
			service.insert(role);
		}else{
			service.update(role);
		}
		return "ok";	
	}
	
	/*
	 * 保存
	 */
	@RequestMapping("/saveRoleUser")
	@ResponseBody
	public String saveRoleUser(HttpServletRequest request,
			@RequestParam(value="id",defaultValue="0") int id,
			@RequestParam(value="users",defaultValue="") String users){
		if ("".equals(users))
			return null;
		
		String[] str = users.split(",");
		//先删除后插入
		service.deleteRoleUsers(id);
		service.insertRoleUsers(str,id);
		
		return "ok";	
	}
	/*
	 * 保存角色的菜单
	 */
	@RequestMapping("/saveMenu")
	@ResponseBody
	public String saveMenu(HttpServletRequest request,
			@RequestParam(value="id",defaultValue="0") int id,
			@RequestParam(value="menus",defaultValue="") String menus){
		if ("".equals(menus))
			return null;
		
		String[] str = menus.split(",");
		//先删除后插入
		service.deleteRoleMenu(id);
		service.insertRoleMenu(str,id);
		
		return "ok";	
	}
	/*
	 * 删除
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(HttpServletRequest request,
			@RequestParam(value="id",defaultValue="0") int id){
		service.delete(id);
		
		return "ok";	
	}
}