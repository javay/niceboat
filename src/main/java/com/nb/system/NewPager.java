package com.nb.system;

import java.util.Map;

import org.nutz.dao.pager.Pager;

import com.nb.common.utils.SearchFilter;

public class NewPager extends Pager {

	public static final String ASC = "asc";
	public static final String DESC = "desc";
	
	private Map<String, SearchFilter> filters;
	private String order;
	private String orderBy;
	
	//---------------------------------

	public Map<String, SearchFilter> getFilters() {
		return filters;
	}

	public void setFilters(Map<String, SearchFilter> filters) {
		this.filters = filters;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	
}
