
package com.nb.common.utils;

import java.util.List;

import org.nutz.dao.Cnd;
import org.nutz.dao.Condition;
import org.nutz.dao.Dao;
import org.nutz.dao.pager.Pager;

import com.nb.article.model.Article;
import com.nb.article.service.ArticleService;
import com.nb.category.model.Category;
import com.nb.category.service.CategoryService;

/**
 * 内容管理工具类
 */
public class CmsUtils {
	
	//private static ArticleService articleService = SpringContextHolder.getBean(ArticleService.class);
	//private static CategoryService categoryService = SpringContextHolder.getBean(CategoryService.class);
	private static Dao dao = SpringContextHolder.getBean(Dao.class);

    /**
     * 得到文章
   	 */
    public static List<Article> getArticles(Integer categoryid,Integer num) {
    	Condition c = Cnd.where("categoryid","=",categoryid);
    	Pager pager = dao.createPager(1, num);
        List<Article> list = dao.query(Article.class, c, pager);
    	return list;
    }
    
    /**
     * 得到文章
   	 */
    public static Article getArticle(Long articleid) {
    	return dao.fetch(Article.class, articleid);
    }    
    
    
    
    
    public static List<Category> getCategory() {
    	
    	return dao.query(Category.class, null);
    }
}