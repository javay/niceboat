﻿package com.nb.category.service;

import java.util.List;

import org.nutz.dao.Dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nb.article.model.Article;
import com.nb.category.model.Category;


/**
 * 文章管理实现类
 * @author 赵占涛 369880281@qq.com
 *
 */
@Service
public class CategoryService {

	@Autowired
	private Dao dao;
	


	public List<Category> getAll() {
		return dao.query(Category.class, null);
	}

	public void add(Article article) {
		
	}

	public void delete(int id) {
		
	}

	public void update(Article article) {
		
	}

	public Article getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
