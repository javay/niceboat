﻿package com.nb.category.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nb.category.model.Category;
import com.nb.category.service.CategoryService;

/**
 * 栏目
 * @author mefly
 *
 */
@Controller
@RequestMapping("/category")
public class CategoryController {

	/**
	 * 文章管理 
	 */
	@Autowired
	private CategoryService service;
	
	/**
	 * 跳转到添加文章，这里基本上是什么都没做，这样是为了以后扩展，比如有了文章分类的话，可以在这里加载分类数据。
	 */
	@RequestMapping("addShow")
	public String addShow() {
		return "categoryAdd";
	}
	/**
	 * 添加文章
	 * @param title 标题
	 * @param content 内容
	 */
	@RequestMapping("add")
	public String add(String title, String content) {
		Category category = new Category();
		return "redirect:list.do";
	}
	
	/**
	 * 显示一篇文章
	 * @param id 文章id
	 * @return 文章 
	 */
	@RequestMapping("/show")
	public String show(int id, HttpServletRequest request) {
		return "categoryShow";		
	}
	
	
	/**
	 * 文章列表
	 */
	@RequestMapping("/list")
	public String list(HttpServletRequest request) {

		request.setAttribute("category_1", "主 页<br><span>HOME</span>");
		return "f_index";
	}
	
	/**
	 * 文章列表
	 */
	@RequestMapping("/list/json")
	@ResponseBody
	public List<Category> list() {
		List<Category> categorys = service.getAll();
		return categorys;
	}
	
	/**
	 * 删除文章
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete")
	public String delete(int id) {
		service.delete(id);
		return "redirect:list.do";
	}
	
}
