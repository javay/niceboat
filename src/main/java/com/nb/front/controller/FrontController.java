﻿package com.nb.front.controller;

import javax.servlet.http.HttpServletRequest;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.nb.article.model.Article;
import com.nb.category.model.Category;

/**
 *
 */
@Controller
public class FrontController {
	@Autowired
	private Dao dao;
	
	/**
	 *
	 * @param title 标题
	 * @param content 内容
	 */
	@RequestMapping("/{url}")
	public String nav(HttpServletRequest request,
			@PathVariable("url") String url) {
		
		Category cate = dao.fetch(Category.class, Cnd.where("url","=",url));
		
		//列表模式
		if ("1".equals(cate.getShowMode())){
			request.setAttribute("categoryid", cate.getId());
			return "jsp/front/list";
		}
		//单页模式
		if ("2".equals(cate.getShowMode())){
			Article art = dao.fetch(Article.class, Cnd.where("categoryid","=",cate.getId()));
			if(art!=null){
				request.setAttribute("articleid", art.getId());
			}
			return "jsp/front/page_contact";
		}
		
		return "jsp/front/"+url;
	}

	@RequestMapping("/{url}/{articleid}.html")
	public String detail(HttpServletRequest request,
			@PathVariable("url") String url,
			@PathVariable("articleid") String articleid) {
		request.setAttribute("articleid", articleid);
		return "jsp/front/page_contact";
	}
}
