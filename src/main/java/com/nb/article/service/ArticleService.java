﻿package com.nb.article.service;

import java.util.List;

import org.nutz.dao.Condition;
import org.nutz.dao.Dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nb.article.model.Article;


/**
 * 文章管理实现类
 * @author 赵占涛 369880281@qq.com
 *
 */
@Service("articleService")
public class ArticleService {

	@Autowired
	private Dao dao;
	


	public List<Article> getAll(Condition cnd) {
		return dao.query(Article.class, cnd);
	}

	public void add(Article article) {
		// TODO Auto-generated method stub
		
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	public void update(Article article) {
		// TODO Auto-generated method stub
		
	}

	public Article getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
