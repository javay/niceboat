<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>

<%@ include file="/jsp/front/header.jsp"%>

	<script type="text/javascript" >

	 $(document).ready(function() {
        $('#coin-slider').coinslider({ width: 990,navigation: true, delay: 5000,effect:"random",links : false  });
    });
	</script>
<!--main-->
	<div class="banner center" >
			<div id='coin-slider'>
		    <a href="img01_url" target="_blank">
		        <img src='${img_path}/slider/01.jpg' >
		       
		    </a>
		    <a href="img01_url" target="_blank">
		        <img src='${img_path}/slider/02.jpg' >
		       
		    </a>
		    <a href="img01_url" target="_blank">
		        <img src='${img_path}/slider/03.jpg' >
		       
		    </a>
		    <a href="img01_url" target="_blank">
		        <img src='${img_path}/slider/04.jpg' >
		       
		    </a>
		    <a href="img01_url" target="_blank">
		        <img src='${img_path}/slider/05.jpg' >
		       
		    </a>
		</div>
	</div>
	<div class="content center">
		<div class="news left">
			<span class="more"><a href="{$CATEGORYS[8]['url']}">more</a></span>
			<ul>
			<c:forEach items="${cms:getArticles(4,15)}" var="it" >
				<li><a href="{$v['url']}" title="{$v['title']}" target="_blank">${it.title}</a></li>
			</c:forEach>
			</ul>
		</div>
		<div class="about left">
			<span class="more"><a href="{$CATEGORYS[6]['url']}">more</a></span>
			<img src="${img_path}/about_img.jpg"/>
			<p>
			{pc:get sql="SELECT * FROM gx_page" catid="6" num="1"}
			{loop $data  $r}
				{str_cut(strip_tags($r[content]),170)}
			{/loop}
			{/pc}
			<a href="{$CATEGORYS[6]['url']}">[更多]</a></p>
		</div>
		<div class="contact left">
			<span class="tel">(+86)0566-5211345</span>
			<p>生产基地：安徽池州经济开发区通港大道特1号</p>
			<p class="add"> 浙江杭州江南经济开发区富春江路18号</p>
			<p>销售总部：安徽池州世纪广场</p>
			<ul class="online">
				<li class="left"><img src="${img_path}/code.jpg" /><li>
				<li class="code left">手机扫描左侧二维码快速获取联系方式<li>
			</ul>
		</div>
	</div>
</div>