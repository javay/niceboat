<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
	<title>{site_title}</title>
	<meta name="keywords" content="{$SEO['keyword']}">
	<meta name="description" content="{$SEO['description']}">
    <link href="${ctx}/static/css/reset_panda.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/static/css/style.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/static/coin-slider/coin-slider-styles.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${ctx}/static/js/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/jquery.sgallery.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/DD_belatedPNG_0.0.8a-min.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/global.js"></script>
	<script type="text/javascript" src="${ctx}/static/coin-slider/coin-slider.js"></script>
	<!--[if IE 6]>
	<script src="DD_belatedPNG.js"></script>
	<script>
	  DD_belatedPNG.fix('.nav,.tips,.about_left h1');
	</script>
	<![endif]-->
</head>
<body>
<div class="web center">
	<div class="tips center">
		<div class="notice left">
			<div id="announ">
			<ul>
				<li><a href="{APP_PATH}index.php?m=announce&c=index&a=show&aid={$r['aid']}" class="bright" target="_blank">{str_cut($r[title],'100')}</a></li>
			</ul>
			</div>
			<script type="text/javascript">
				$(function(){
					startmarquee('announ',40,1,500,4000);
				})
			</script>
		</div>
			<div class="setup right">
			<ul>
				<li><a href="#" id="sethomepage">设为首页</a>|<a href="#" id="favorites">加入收藏</a></li>
			</ul>
		</div>
	</div>
	<div class="header center">
		<a href="{siteurl($siteid)}"><img src="${img_path}/logo.jpg" /></a>
		<ul class="nav">
		<c:forEach items="${cms:getCategory()}" var="it" >
		<li class="left"><a href="${ctx}/${it.url}">${it.html}</a></li>
		</c:forEach>
		</ul>
	</div>