<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>
<script type="text/javascript">
$(function (){
	ligergrid =  $("#maingrid").ligerGrid({
            columns: 
				[ 
	              {display : '模块',name : 'module'}, 
	              {display : '描述',name : 'description'}, 
	              {display : '顺序',name : 'sx'}, 
	              {
                     display: '操作', isAllowHide: false,
                     render: function (row)
                     {
                         var html ="";
                        html+='<button class="btn btn-mini btn-info" onclick="f_edit({0})" type="button">编辑</button>&nbsp;';
                         html+='<button class="btn btn-mini btn-danger" onclick="f_delete({0})" type="button">删除</button>';
                        return Free.replace(html,row.id);
                     }, width: 160
                 }

            ],
            url: '${ctx}/system/authorize/ajax_list.do',
            pageSize: 10, sortName: 'id',
            width: '99%', height: '98%', 
            checkbox : false,
            pageParmName:'page'
        });

    });
function f_query(){
	ligergrid.set('parms',$('#query-form').serializeArray());
	ligergrid.loadData();
}
function f_edit(id){
  var newDialog = new Dialog({type:'url',value:'${ctx}/system/user/input.do?id='+id},
				{modal:true,width:950,height:350}).show();
}

function f_add(){
	var newDialog = new Dialog({type:'url',value:'${ctx}/jsp/system/input-user.jsp'},
		{modal:true,width:950,height:350}).show();
}
function f_delete(id){
	  freeConfirm("是否将此信息删除?",function(){
	  Free.ajax({
       	   url: '${ctx}/system/user/delete.do',
       	   data: {id:id},
       	   success: function(data){
       		   if (data='ok'){
       			 ligergrid.loadData();
       		   }
        	   }
	  });
	});
}
</script>
</head>
<body>
	<div id="maingrid" style="margin: 0; padding: 0">
		<form action="">
			<table class="tab">
	          <tr class="tab_grey02">
	            <td>
				</td>
	          </tr>
	          <tr class="tab_white02">
	            <td>
		            <a class="red_but fGridSearch" href="javascript:void(0);">查询</a>
		            <a class="red_but freeGrid_new" href="javascript:void(0);">新增</a>
	            </td>
	          </tr>
	        </table>
		</form>
	</div>
</body>	
</html>
