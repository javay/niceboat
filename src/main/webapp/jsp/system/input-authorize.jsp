<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<script>

$("#inputForm").form({callback:function(){
	g.reload();//重新加载grid
	newDialog.close();
}});

$(function(){
	
});
</script>


<form id="inputForm" action="${ctx}/system/authorize/save.do">

<input type="hidden" name="id" value="${ob.id}" />
<div class="content">
	<p>
		<label>模块：</label>
		<input name="module" value="${ob.module}" type="text" label="模块" maxlength="100" />
	</p>
	<p>
		<label>描述：</label>
		<input name="description" value="${ob.description}" type="text" label="描述" maxlength="50" />
	</p>
	<p>
		<label>顺序：</label>
		<input name="sx" value="${ob.sx}" type="text" label="顺序" maxlength="4" />
	</p>
</div>

<a class="red_but freeGrid_submit" href="javascript:void(0);">保存</a>
</form>
