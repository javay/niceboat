<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<script>

$("#inputForm").form({callback:function(){
	if (zTree){
		zTree.reAsyncChildNodes(null, "refresh");
	}
}});

$(function(){
	if ("${param.pid}"!=""){
		$("#pid").val("${param.pid}");
	}
	
});
</script>

<style>
.content input{
width:300px;
}
.content p{
width:500px;
}
</style>
<form id="inputForm" action="${ctx}/system/menu/save.do">

<input type="hidden" name="id" value="${ob.id}" />
<div class="content">
	<p>
		<label>菜单名称：</label>
		<input name="menuname" value="${ob.menuname}" type="text" label="菜单名称" maxlength="50" />
	</p>
	<p>
		<label>菜单url：</label>
		<input name="url" value="${ob.url}" type="text" label="菜单url" maxlength="100" />
	</p>
	<p>
		<label>父级菜单ID：</label>
		<input id="pid" name="pid" value="${ob.pid}" type="text" label="父级菜单ID" maxlength="20" />
	</p>
	<p>
		<label>是否显示：</label>
		<input name="sfxs" value="${ob.sfxs}" type="text" label="是否显示" maxlength="2" />
	</p>
	<p>
		<label>顺序：</label>
		<input name="sx" value="${ob.sx}" type="text" label="顺序" maxlength="2" />
	</p>
	<p style="text-align: center">
		<button class="btn btn-success" id="free_submit" type="button"> 保存 </button>
		<button class="btn" type="reset"> 重置 </button>
	</p>
</div>

</form>
