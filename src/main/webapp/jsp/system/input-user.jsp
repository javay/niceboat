<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<script>

$(function(){
	Free.validateSubmitAndClose($("#inputForm"));

	$("#freeLookup").click(function(){
		lookupDialog = new Dialog({type:'url',value:'${ctx}/jsp/system/tree-department-select.jsp?lookupId='+$(this).attr("lookupId")+"&lookupName="+$(this).attr("lookupName")},
				{modal:true});
		lookupDialog.show();
	});

});
</script>
<form  class="span12" id="inputForm"  action="${ctx}/system/user/save.do">
<input type="hidden" name="id" value="${ob.id}" />
  <div class="row">
     <legend>个人信息</legend><br/>
    <label class="span2 " for="">登录名：</label><div class="span4">
		<input name="loginname" id="loginname" placeholder="必添项" value="${ob.loginname}" type="text" validate="{required:true}"  />
	</div>
		<label class="span2 " for="">部门：</label><div class="span4">
		<input name="deptid" id="deptid" value="${ob.deptid}" type="hidden"  />
	   <div class="input-append">
		 	<input class="input-medium"  name="deptname" id="deptname" readonly="readonly"  value="${ob.deptname}" type="text"  />
			<a id="freeLookup" class="btn" lookupId="deptid" lookupName="deptname" href="javascript:void(0);">选择</a>
		</div>
	</div>
	
		<label class="span2 " for="">代表证号：</label><div class="span4">
		<input name="dbzh" value="${ob.dbzh}" type="text" label="代表证号" />
	</div>
	
		<label class="span2 " for="">姓名：</label><div class="span4">
		<input name="xm" value="${ob.xm}" placeholder="必添项"  type="text" label="姓名" validate="{required:true}"/>
	</div>
	
		<label class="span2 " for="">性别：</label><div class="span4">
		<input name="xb" value="${ob.xb}" type="text" label="性别" />
	</div>
	
		<label class="span2 " for="">民族：</label><div class="span4">
		<input name="mz" value="${ob.mz}" type="text" label="民族" />
	</div>
	
		<label class="span2 " for="">文化程度：</label><div class="span4">
		<input name="whcd" value="${ob.whcd}" type="text" label="文化程度" />
	</div>
	
		<label class="span2 " for="">政治面目：</label><div class="span4">
		<input name="zzmm" value="${ob.zzmm}" type="text" label="政治面目" />
	</div>
	
		<label class="span2 " for="">代表团：</label><div class="span4">
		<input name="dbt" value="${ob.dbt}" type="text" label="代表团" />
	</div>
	
		<label class="span2 " for="">文件送达地：</label><div class="span4">
		<input name="wjsdd" value="${ob.wjsdd}" type="text" label="文件送达地" />
	</div>
	
		<label class="span2 " for="">技术支撑：</label><div class="span4">
		<input name="jszc" value="${ob.jszc}" type="text" label="技术支撑" />
	</div>
	
		<label class="span2 " for="">职业：</label><div class="span4">
		<input name="zy" value="${ob.zy}" type="text" label="职业" />
	</div>
	
		<label class="span2 " for="">职务：</label><div class="span4">
		<input name="zw" value="${ob.zw}" type="text" label="职务" />
	</div>
	
		<label class="span2 " for="">单位名称：</label><div class="span4">
		<input name="dwmc" value="${ob.dwmc}" type="text" label="单位名称" />
	</div>
	
		<label class="span2 " for="">单位电话：</label><div class="span4">
		<input name="dwdh" value="${ob.dwdh}" type="text" label="单位电话" />
	</div>
	
		<label class="span2 " for="">单位地址：</label><div class="span4">
		<input name="dwdz" value="${ob.dwdz}" type="text" label="单位地址" />
	</div>
	
		<label class="span2 " for="">单位邮编：</label><div class="span4">
		<input name="dwmb" value="${ob.dwmb}" type="text" label="单位邮编" />
	</div>
	
		<label class="span2 " for="">电子邮政：</label><div class="span4">
		<input name="dzyz" value="${ob.dzyz}" type="text" label="电子邮政" />
	</div>
	
		<label class="span2 " for="">传真：</label><div class="span4">
		<input name="cz" value="${ob.cz}" type="text" label="传真" />
	</div>
	
		<label class="span2 " for="">家庭住址：</label><div class="span4">
		<input name="jtzz" value="${ob.jtzz}" type="text" label="家庭住址" />
	</div>
	
		<label class="span2 " for="">家庭邮编：</label><div class="span4">
		<input name="jtyb" value="${ob.jtyb}" type="text" label="家庭邮编" />
	</div>
	
		<label class="span2 " for="">家庭电话：</label><div class="span4">
		<input name="jtdh" value="${ob.jtdh}" type="text" label="家庭电话" />
	</div>
	
		<label class="span2 " for="">移动电话：</label><div class="span4">
		<input name="yddh" value="${ob.yddh}" type="text" label="移动电话" />
	</div>
	
		<label class="span2 " for="">是否调出或逝世：</label><div class="span4">
		<input name="sfdc" value="${ob.sfdc}" type="text" label="是否调出或逝世" />
	</div>
	<p >
		<label class="span2 " for="">备注：</label><div class="span4">
		<input name="memo" value="${ob.memo}" type="text" label="备注" />
	</div>
	
		<label class="span2 " for="">照片：</label><div class="span4">
		<input name="zp" value="${ob.zp}" type="text" label="照片" />
	</div>
	
		<label class="span2 " for="">是否启用：</label><div class="span4">
		<input name="sfqy" value="${ob.sfqy}" type="text" label="是否启用" />
	</div>
	
    <div class="span12 text-center">
	    <div class="form-actions">
		  <button id="free_submit" type="submit" class="btn btn-success">保存</button>
		  <button type="reset" class="btn">重置</button>

		</div>
    </div>
  </div>
</form>
