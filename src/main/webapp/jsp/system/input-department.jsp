<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<script>

$("#inputForm").form({callback:function(){
	if (treeObj){
		treeObj.reAsyncChildNodes(null, "refresh");
	}
}});

$(function(){
	if ("${param.fid}"!=""){
		$("#fid").val("${param.fid}");
	}
});
</script>
<form method="post" id="inputForm" action="${ctx}/system/department/save.do" >
<input type="hidden" name="id" value="${ob.id}" />
<div class="content">
	<p>
		<label>部门名称：</label>
		<input name="deptname" value="${ob.deptname}" type="text" validate="{required:true}" label="部门名称" maxlength="30" />
	</p>
	<p>
		<label>部门电话：</label>
		<input name="deptphone" value="${ob.deptphone}" type="text" label="部门电话" maxlength="20" />
	</p>
	<p>
		<label>部门地址：</label>
		<input name="deptadress" value="${ob.deptadress}" type="text" label="部门地址" maxlength="30" />
	</p>
	<p>
		<label>部门负责人：</label>
		<input name="deptfzr" value="${ob.deptfzr}" type="text" label="部门负责人" maxlength="10" />
	</p>
	<p>
		<label>部门联系人：</label>
		<input name="deptlxr" value="${ob.deptlxr}" type="text" label="部门联系人" maxlength="10" />
	</p>
	<p>
		<label>父级部门ID：</label>
		<input name="fid" id="fid" value="${ob.fid}" type="text" label="父级部门ID" maxlength="0" />
	</p>
	<p style="text-align: center">
		<button class="btn btn-success" id="free_submit" type="button"> 保存 </button>
		<button class="btn" type="reset"> 重置 </button>
	</p>
</div>

</form>
