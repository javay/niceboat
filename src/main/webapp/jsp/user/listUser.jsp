<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>NiceBoat CMS </title>
    <link href="${ligerUIUrl}/lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="${ligerUIUrl}/lib/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" /> 
    <link href="${ligerUIUrl}/lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" type="text/css" id="mylink"/>      
    <script src="${ctx}/ui/jquery-2.0.3.min.js"></script> 
    <script src="${ligerUIUrl}/lib/ligerUI/js/ligerui.all.js" type="text/javascript"></script> 
    <script src="${ligerUIUrl}/lib/json2.js"></script>
    <script src="${ctx}/ui/layer/layer.min.js"></script>
    
    <script type="text/javascript">
		var g;
        var bm;
        var filds =[ 
                    { display: 'ID', name: 'id', width: 80},
                    { display: '登录名', name: 'loginname', width: 100},
                    { display: '用户名', name: 'username', width: 100},
                    {display: 'Email', name: 'email',  width: 120 } ,
                    {display: '手机', name: 'phone', width: 120 }
                    ];
        $(function ()
        {
            bm = $("#btn").ligerButton(
                    {
                        click: function ()
                        {
                    		f_search();
                        }
                    }
              );
            bm.setValue('查询');
            bm.set("width", "40");
            g = $("#maingrid4").ligerGrid({
                columns: filds,
                //delayLoad:true,
                root:'items',
                record :'totalCount',
                dataAction: 'server',
                pageSize:10, 
                data: $.extend(true,{},filds), 
                url:"${ctx}/user/list/json.do",  
                width: '98%', height: '98%', rownumbers:true, 
                onDblClickRow : function (data, rowindex, rowobj)
                {
                    //alert('选择的是');
                },pageSizeOptions:[10, 20, 30, 40, 50],
                toolbar: { items: [
	                       { text: '创建', click: itemclick, icon: 'add' },
	                       { line: true },
	                       { text: '修改', click: itemclick, icon: 'modify' },
	                       { line: true },
	                       { text: '删除', click: itemclick, icon: 'delete' }
	                       ]
	                       }
            });
            g.dataAction='server';
            $("#pageloading").hide();
        });
        
        function itemclick(item)
        {
            var row = g.getSelectedRow();
        	if(item.text=='创建'){
        	 parent.$.layer({
        	        type: 2,
        	        title: '信息编辑',
        	        maxmin: true,
        	    	zIndex: parent.layer.zIndex,
        	        shadeClose: true, //开启点击遮罩关闭层
        	        area : ['600px' , '400px'],
        	        offset : ['100px', ''],
        	        iframe: {src: '${ctx}/jsp/user/input-user.jsp?winid='+window.name}
        	    });
        	} 
            if (!row && item.text!='创建') { layer.msg('请选择行', 2, -1); return; }
        	if(item.text=='修改'){
           	 parent.$.layer({
           	        type: 2,
           	        title: '信息编辑',
           	        maxmin: true,
           	    	zIndex: parent.layer.zIndex,
           	        shadeClose: true, //开启点击遮罩关闭层
           	        area : ['600px' , '400px'],
           	        offset : ['100px', ''],
           	        iframe: {src: '${ctx}/user/input.do?winid='+window.name+"&id="+row.id}
           	    });
           	}
        	if(item.text=='删除'){
        		$.layer({
        		    area: ['auto','auto'],
        		    dialog: {
        		        msg: '确定要删除些数据吗？',
        		        btns: 2,                    
        		        type: 4,
        		        btn: ['是','否'],
        		        yes: function(){
        		        	$.ajax({
	     		  			   type: "POST",
	    		 			   url: "${ctx}/user/delete.do?id="+row.id,
	    		 			   success: function(data){
	    		 					if(data=="ok"){
	    	        		        	layer.msg('删除成功', 1, 1);
	    	        		        	f_search();
	    		 					}
	    		 			   },
		               			error:function(xhr, ajaxOptions, thrownError){
		            				$("#msg").html("<div >Http status: " + xhr.status + " " + xhr.statusText + "" 
		                					+ "ajaxOptions: "+ajaxOptions + ""
		                					+ "thrownError: "+thrownError + ""
		                					+ ""+xhr.responseText+"</div>");
		                		    $.ligerDialog.open({ width:600, height:300,target: $("#msg") });
		            			},
	    					   async: false
	    		 			});
        		        }, no: function(){
        		        }
        		    }
        		});
        	}
        }
        function f_search()
        { 	
            g.setOptions(
                { parms: [
                          { name: 'filter_LIKES_loginname', value: $("input[name='filter_LIKES_loginname']").val()},
                          ] }
            );
            g.loadData(true);
        }
    </script>
</head>
<body>
<table><tr><td>登录名称：<input type="text" id="filter_LIKES_loginname"
		name="filter_LIKES_loginname" value="${param.filter_LIKES_loginname}"
		class="search_text_input" style="width: 150px; height: 15px" />
			</td>
		<td>
			<div id="btn"></div>
		</td>
		
		</tr></table>
	
		
		
    <div class="l-loading" style="display: block" id="pageloading">
    </div> 
    <div id="maingrid4" style="margin: 0; padding: 0">
    </div>
    <div style="display: none;">
    </div>
<div id="msg" style="display:none" ></div>
</body>
</html>