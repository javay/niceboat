<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>NiceBoat CMS </title>
    <link href="${ligerUIUrl}/lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" /> 
    <link href="${ligerUIUrl}/lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" type="text/css" id="mylink"/>       
    <script src="${ctx}/ui/jquery-2.0.3.min.js"></script>    
    <script src="${ligerUIUrl}/lib/ligerUI/js/ligerui.all.js" type="text/javascript"></script> 
    <script src="${ligerUIUrl}/lib/json2.js"></script>
    <script src="${ctx}/ui/layer/layer.min.js"></script>
    <script src="${ctx}/ui/jquery.form.js"></script>
    <script src="${ctx}/ui/jquery-validation/jquery.metadata.js"></script>
    <script src="${ctx}/ui/jquery-validation/jquery.validate.min.js"></script>
    <script src="${ctx}/ui/jquery-validation/messages_cn.js"></script>
    <script type="text/javascript">
    //关闭当前并刷新列表
	function closeAndRefresh(){
		window.parent.frames['${param.winid}'].f_search();
		parent.layer.close(index); 
	}
    var a="test";
	var index = parent.layer.getFrameIndex(window.name);
    $(function () {	
    	
        $("form").ligerForm();
        $.metadata.setType("attr", "validate");
        var v = $("form").validate({
            debug: true,
            errorPlacement: function (lable, element)
            {
                if (element.hasClass("l-textarea"))
                {
                    element.ligerTip({ content: lable.html(), target: element[0] }); 
                }
                else if (element.hasClass("l-text-field"))
                {
                    element.parent().ligerTip({ content: lable.html(), target: element[0] });
                }
                else
                {
                    lable.appendTo(element.parents("td:first").next("td"));
                }
            },
            success: function (lable)
            {
                lable.ligerHideTip();
                lable.remove();
            },
            submitHandler: function ()
            {
                $("form .l-text,.l-textarea").ligerHideTip();
                var options = { 
            			url:'${ctx}/user/save.do', //提交给哪个执行 
            			type:'POST', 
            			success: function(data){ 
            				if (data=='ok'){//显示操作提示 
            					//alert('保存成功');
            					layer.alert('保存成功',1,function(){
            						closeAndRefresh();
            		        	});
            				}
            			},
            			error:function(xhr, ajaxOptions, thrownError){
            				$("#msg").html("<div >Http status: " + xhr.status + " " + xhr.statusText + "" 
                					+ "ajaxOptions: "+ajaxOptions + ""
                					+ "thrownError: "+thrownError + ""
                					+ ""+xhr.responseText+"</div>");
                		    $.ligerDialog.open({ width:600, height:300,target: $("#msg") });
            			}
            		}; 
            	$('#inputForm').ajaxSubmit(options); 
            	return false; 
            }
        });
        $('#new').on('click', function(){
            $.layer({
                type : 2,
                title : 'iframe子父操作，该窗口创建于：'+ window.name,
                fix: false,
                iframe : {src : '${ctx}/jsp/user/dialog.jsp'},
                shadeClose: true,
                maxmin: true,
                zIndex: parent.layer.zIndex,
                area: ['1000px' , '500px'],
                offset : ['100px', ''],
                close : function(index){
                    layer.msg('您获得了子窗口标记：' + layer.getChildFrame('#aa', index).val(),3,1);
                }
            });
        });
    });
    </script>
    <style type="text/css">
           body{ font-size:12px;}
        .l-table-edit {}
        .l-table-edit-td{ padding:4px;}
        .l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
        .l-verify-tip{ left:230px; top:120px;}
    </style>
</head>
<body>

<form  id="inputForm">
<input type="hidden" name="thisid" value="${ob.id}" />
	   <table cellpadding="0" cellspacing="0" class="l-table-edit" >
            <tr>
                <td align="right" class="l-table-edit-td">登录名:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="loginname" value="${ob.loginname}" type="text" id="txtName" ltype="text" validate="{required:true,minlength:3,maxlength:50}" /></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">用户名:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="username" value="${ob.username}" type="text" id="txtName" ltype="text" validate="{required:true,maxlength:50}" /></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">email:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="email" value="${ob.email}" type="text" id="txtName" ltype="text" validate="{maxlength:50}" /></td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">phone:</td>
                <td align="left" class="l-table-edit-td" style="width:160px"><input name="phone" value="${ob.phone}" type="text" id="phone" ltype="text" validate="{maxlength:20}" /></td>
                <td align="left"></td>
            </tr>
	</table>
	<input type="button" value="new" id="new" >
<input type="submit" value="提交" id="Button1" class="l-button l-button-submit" /> 
<div id="msg" style="display:none" ></div>
</form>
</body>
</html>