/*


*/
;(function ($) {
	function aa(){
		alert();
	}
	function getData(opts,$this,searchOpts){
    	//$("#g1 tbody",$this).html("");//清空原数据
        var tbody=""
        var parame = {page:opts.page,pagesize:opts.pagesize};
        
        if (searchOpts){
	        $.each(searchOpts,function(i,o){
	        	parame['filter_LIKES_'+o.name]=o.value;
	        });
        }
        console.info(parame);
        $.ajax({
     	   type: "POST",
     	   url: opts.url,
     	   data: parame,
     	   success: function(data){
     		
              $.each(data.items,function(i,o){
               	$.each(opts.filds,function(j,f){
               		$this.find("tbody").children().eq(i).children().eq(j).text(o[f.name]);
               		if (f.name==''){
                   		$this.find("tbody").children().eq(i).children().eq(j).html(
                   				"<a href='#' id='"+o['id']+"'>编辑</a> <a href='#' id='"+o['id']+"'>删除</a>");
               		}
                  });
               });
             $("#g1",$this).append(tbody);
             if(data.page==1){//第一页时
            	 $("#firstPage",$this).hide();
            	 $("#prePage",$this).hide();
             }else{
            	 $("#firstPage",$this).show();
            	 $("#prePage",$this).show();
             }
             if(data.page==data.totalPages){
            	 $("#lastPage",$this).hide();
            	 $("#nextPage",$this).hide();
             }else{
            	 $("#lastPage",$this).show();
            	 $("#nextPage",$this).show();
             }
        	 $("#totalPages",$this).text(data.totalPages);
        	 $("#page",$this).text(data.page);
        	 $("#totalCount",$this).text(data.totalCount);
     	   }
     	});
	}
    $.fn.grid = function (options) {
        var defualts = {page:1,pagesize:10};
        var opts = $.extend({}, defualts, options);
        var $this =$(this);
        var th="<thead>"
        $.each(opts.filds,function(i,o){
        	th+="<th style='width:"+o.width+"px' >"+o.display+"</th>";
        });
        th+="</thead>";
        for (var j = 0; j < opts.pagesize; j++) {
        	th+="<tr>";
        	$.each(opts.filds,function(i,o){
            	th+="<td ></td>";
            });
        	th+="</tr>";
        }
        
        var table="<table border='1' id='g1' >"+
        th+
        "</table><div><label id='firstPage'><a>第一页</a></label>" +
        "<label id='prePage'><a>上一页</a></label>" +
        "<label id='nextPage'><a>下一页</a></label>" +
        "<label id='lastPage'><a>最后一页</a></label>" +
        "<label id='page'></label>/" +
        "<label id='totalPages'></label>" +
        "共<label id='totalCount'></label>条记录" +
        "</div>";
        $this.append(table);
        getData(opts,$this);
        
        $("#nextPage",$this).click(function(){
        	opts.page+=1;
            getData(opts,$this);
        });
        $("#prePage",$this).click(function(){
        	opts.page-=1;
            getData(opts,$this);
        });
        $("#firstPage",$this).click(function(){
        	opts.page=1;
            getData(opts,$this);
        });
        $("#lastPage",$this).click(function(){
        	opts.page=$("#totalPages").text();
            getData(opts,$this);
        });
        $this.find("form .gridSearch").click(function(){
        	
            getData(opts,$this,$this.find("form").serializeArray());
        })
        
    };
   
})(jQuery);