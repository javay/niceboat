/**
 * Free UI 的核心共用方法
 *
 */
var Free={
	ajaxError:function(xhr, ajaxOptions, thrownError){
		if (Dialog){
			new Dialog("<div>Http status: " + xhr.status + " " + xhr.statusText + "</div>" 
					+ "<div>ajaxOptions: "+ajaxOptions + "</div>"
					+ "<div>thrownError: "+thrownError + "</div>"
					+ "<div>"+xhr.responseText+"</div>",{title:"程序出错",width:1000,height:360}).show();
		}else{
			alert("Http status: " + xhr.status + " " + xhr.statusText + "\najaxOptions: " + ajaxOptions + "\nthrownError:"+thrownError + "\n" +xhr.responseText);	
		}
		
	},
	ajax:function(par){
		var as =true;
		if (par.async){
			as = par.async;
		}
		 $.ajax({
			   type: "POST",
			   url: par.url,
			   data: par.data,
			   success: par.success,
			   async: as,
			   error:Free.ajaxError
		   });
	},
	//关闭当前的对话框，方便使用
	closeThisDialog:function(){	
		//查询当前所有dialog并取dialogid最大的一个，也就是最前面的dialog并关闭
		var did =0;
		$('.dialog').each(function(i){
			if(did < (this.id.substring(7)*1))
				did = (this.id.substring(7)*1);
		 })
	    $('#dialog-'+did).fadeOut('slow', function(){$(this).remove();});
	    $('#dialog-'+did+"-overlay").fadeOut('slow', function(){$(this).remove();});
	},
	//字符串格式化函数String.format
	replace : function(src) {
		if (arguments.length == 0)
			return null;
		var args = Array.prototype.slice.call(arguments, 1);
		return src.replace(/\{(\d+)\}/g, function(m, i) {
			return args[i];
		});
	},
	validateSubmitAndClose:function(inputForm){

        $.metadata.setType("attr", "validate");
		inputForm.validate({
		        submitHandler:function(form){ 
		          	$.ajax({
		       		   type: "POST",
		       		   url: inputForm.attr("action"),
		       		   data: inputForm.serialize(),
		      			error:Free.ajaxError,
		       		   success: function(data){
		       	     	if(data=='ok'){
		       	     		alert("保存成功");  
		       	     		if(f_query){
		       	    	 		f_query();//重新加载grid
		       	    	 	}
		       	    	 	Free.closeThisDialog();
		       	     	}
		       		}
		         });
		        }    
		    });
	}
};