<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>  
<link rel="stylesheet" type="text/css"  href="${ctx}/ui/free/dialog.css" />


<link href="${ctx}/ui/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${ctx}/ui/css/font-awesome.min.css" />

<link rel="stylesheet" href="${ctx}/ui/css/jquery-ui-1.10.3.full.min.css" />
<link rel="stylesheet" href="${ctx}/ui/css/datepicker.css" />
<link rel="stylesheet" href="${ctx}/ui/css/ui.jqgrid.css" />

<link rel="stylesheet" href="${ctx}/ui/css/ace.min.css" />
<link rel="stylesheet" href="${ctx}/ui/css/ace-rtl.min.css" />
<link rel="stylesheet" href="${ctx}/ui/css/ace-skins.min.css" />

<script src="${ctx}/ui/js/ace-extra.min.js"></script>
<script src="${ctx}/ui/js/jquery-1.10.2.min.js"></script>

<script src="${ctx}/ui/js/bootstrap.min.js"></script>
<script src="${ctx}/ui/js/typeahead-bs2.min.js"></script>

<!-- page specific plugin scripts -->

<script src="${ctx}/ui/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="${ctx}/ui/js/jqGrid/jquery.jqGrid.min.js"></script>
<script src="${ctx}/ui/js/jqGrid/i18n/grid.locale-en.js"></script>

<!-- ace scripts -->

<script src="${ctx}/ui/js/ace-elements.min.js"></script>
<script src="${ctx}/ui/js/ace.min.js"></script>
		
<script src="${ctx}/ui/free/free.core.js"></script>
<script src="${ctx}/ui/free/free.dialog.js"></script>

<!-- zTree插件 -->
<link href="${ctx}/ui/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/ui/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${ctx}/ui/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${ctx}/ui/ztree/js/jquery.ztree.exedit-3.5.js"></script>